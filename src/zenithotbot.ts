import TelegramBot from 'node-telegram-bot-api'
import winston from "winston"
import { Low } from "lowdb"

import { SportsScraping, SportsruSchema } from './scrapers/sports'
import * as CommandHandlers from './commands'
import { HTMLScraper } from './scrapers/html'

export class ZenitHotBot {
    private logger: winston.Logger
    private telegramBot: TelegramBot
    private sportsScraper: SportsScraping
    private htmlScraper: HTMLScraper
    private mappingDb: Low<SportsruSchema>

    constructor(logger: winston.Logger, token: string, sportsScraper: SportsScraping, htmlScraper: HTMLScraper, mappingDb: Low<SportsruSchema>) {
        this.htmlScraper = htmlScraper
        this.logger = logger
        this.telegramBot = new TelegramBot(token, { polling: true })
        this.sportsScraper = sportsScraper
        this.mappingDb = mappingDb
    }

    activate() {
        this.initializeHooks()
    }

    private async initializeHooks() {
        this.logger.verbose("ZenitHotBot: initializing hooks")

        let handlers: CommandHandlers.CommandHandling[] = [
            CommandHandlers.AliasHandler,
            CommandHandlers.BabichHandler,
            CommandHandlers.HeadToHeadHandler,
            CommandHandlers.LastHandler,
            CommandHandlers.LeagueHandler,
            CommandHandlers.MayakHandler,
            CommandHandlers.NextHandler,
            CommandHandlers.NewsHandler,
            CommandHandlers.SpartakHandler,
            CommandHandlers.StandingsHandler,
            CommandHandlers.VVHandler,
            CommandHandlers.WhenHandler,
            CommandHandlers.WhyHandler
        ].map((classConstructor) => {
            return new classConstructor(this.logger, this.telegramBot, this.sportsScraper, this.htmlScraper, this.mappingDb)
        })

        handlers.forEach((commandHandler: CommandHandlers.CommandHandling) => {
            this.telegramBot.onText(commandHandler.getCommandPattern(), (msg, match) => commandHandler.handleCommand(msg, match))
        })

        setInterval(this.updateMayak, 86400000);
    }

    // MARK: - Hooks

    private async updateMayak() {
        const mayakHandler = new CommandHandlers.MayakHandler(this.logger, this.telegramBot, this.sportsScraper, this.htmlScraper, this.mappingDb)
        mayakHandler.reloadMayakData()
    }
}
