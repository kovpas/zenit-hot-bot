import { Promise } from 'core-js'
import Iconv from 'iconv-lite'
import { URL, format } from 'url'
import { Logger } from 'winston'
import { HTMLDocument, parseHtmlAsync } from 'libxmljs'
import { HTTPClient } from './httpclient'

export interface HTMLScraping {
    rootNodeForPage(url: URL): Promise<HTMLDocument>
}

export class HTMLScraper implements HTMLScraping {
    private encoding: string
    private logger: Logger
    private httpClient: HTTPClient

    constructor(logger: Logger, httpClient: HTTPClient, encoding: string) {
        this.logger = logger
        this.httpClient = httpClient
        this.encoding = encoding
    }

    async rootNodeForPage(url: URL): Promise<HTMLDocument> {
        try {
            const body: Buffer = await this.httpClient.get(format(url), {encoding: null})
            const utf8Body = Iconv.decode(body, this.encoding)
            const document = await parseHtmlAsync(utf8Body)
            return document
        } catch (error) {
            return Promise.reject(error)
        }
    }
}