import { Logger } from 'winston'
import fetch from 'node-fetch'

export interface HTTPClient {
    get(url: string, options: any): Promise<ArrayBuffer>
}

export class HTTPClientImpl implements HTTPClient {
    private logger: Logger

    constructor(logger: Logger) {
        this.logger = logger
    }

    async get(url: string, options: any): Promise<ArrayBuffer> {
        this.logger.debug("Requesting url: " + url)
        const body = (await fetch(url)).arrayBuffer()
        return body
    }
}