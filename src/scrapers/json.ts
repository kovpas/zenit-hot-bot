import Iconv from 'iconv-lite'
import { Url, format } from 'url'
import { Logger } from 'winston'
import { HTTPClient } from './httpclient'

export interface JSONScraping {
    loadJson(url: Url): Promise<any>
}

export class JSONScraper implements JSONScraping {
    private encoding: string
    private logger: Logger
    private httpClient: HTTPClient

    constructor(logger: Logger, httpClient: HTTPClient, encoding: string) {
        this.logger = logger
        this.httpClient = httpClient
        this.encoding = encoding
    }

    async loadJson(url: Url): Promise<any> {
        const body: Buffer = await this.httpClient.get(format(url), {encoding: null, strictSSL: false})
        const utf8Body = Iconv.decode(body, this.encoding)
        const result = JSON.parse(utf8Body)
        return result
    }
}