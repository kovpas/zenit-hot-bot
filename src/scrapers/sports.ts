import { parse } from 'url'
import { Logger } from 'winston'
import moment from 'moment'
import 'moment/locale/ru'

import { HTMLScraping } from './html'
import { League, StandingRow, Round } from '../models/league'
import { Team } from '../models/team'
import { Match, MatchType } from '../models/match'
import { JSONScraping } from './json'
import { News } from '../models/news'

const baseUrl: string = "https://www.sports.ru"
const baseJSONUrl: string = baseUrl + "/stat/export/iphone/"

export interface SportsScraping {
    getLeague(leagueId: number): Promise<League>
    getHeadToHead(teamA: number, teamB: number, number: number): Promise<Match[]>
    getNews(tagId: number, number: number): Promise<News[]>
    getTeam(teamId: number): Promise<Team>
    getTeamDefs(leagueId: number): Promise<SportsruObject[]>
}

export interface SportsruObject {
    id: number
    name: string
    tag_id: number
}

export interface SportsruAlias {
    name: string
    sportsruName: string
}

export interface SportsruSchema {
    leagues: SportsruObject[]
    teams: SportsruObject[]
    aliases: SportsruAlias[]
}

export class SportsScraper implements SportsScraping {
    private logger: Logger
    private htmlScraper: HTMLScraping
    private jsonScraper: JSONScraping

    constructor(logger: Logger, htmlScraper: HTMLScraping, jsonScraper: JSONScraping) {
        this.logger = logger
        this.htmlScraper = htmlScraper
        this.jsonScraper = jsonScraper
    }

    async getHeadToHead(teamA: number, teamB: number, number: number): Promise<Match[]> {
        this.logger.verbose("SportsScraper: getHeadToHead " + teamA + " - " + teamB + " - " + number)
        const headToHeadData = await this.jsonScraper.loadJson(parse(baseJSONUrl + "teams_matches.json?first_team_id=" + teamA + "&second_team_id=" + teamB + "&count=" + number))
        const matches = headToHeadData["matches"].map((h2hData: any) => {
            const id = h2hData["id"] ?? 0
            const type = SportsScraper.tournamentNameToMatchType(h2hData["tournament"]["name"])
            const date = new Date(h2hData["start"] * 1000)
            const homeTeam = new Team(h2hData["first_team"]["name"], [])
            const awayTeam = new Team(h2hData["second_team"]["name"], [])
            const homeGoals = Number(h2hData["first_team"]["goals"])
            const awayGoals = Number(h2hData["second_team"]["goals"])
            const isPenalty = h2hData["win"] != "draw" && (homeGoals == awayGoals)
            let homePenalty = undefined; if (isPenalty) { homePenalty = h2hData["win"] == "first" ? 3 : 0 }
            let awayPenalty = undefined; if (isPenalty) { awayPenalty = h2hData["win"] == "second" ? 3 : 0 }
            return new Match(id, type, date, homeTeam, awayTeam, false,
                             homeGoals, awayGoals, homePenalty, awayPenalty)
        })

        return matches
    }

    async getTeam(teamId: number): Promise<Team> {
        this.logger.verbose("SportsScraper: getTeam " + teamId)
        const teamData = await this.jsonScraper.loadJson(parse(baseJSONUrl + "teaminfo.json?team=" + teamId))
        const teamCalendar: [any] = await this.jsonScraper.loadJson(parse(baseJSONUrl + "team_full_calendar.json?team=" + teamId))
        const team = new Team(teamData["name"], [])
        team.matches = await Promise.all(teamCalendar.map((m: any) => this.parseMatch(m)))
        this.logger.verbose("SportsScraper: got the team")

        return team
    }

    async getLeague(leagueId: number): Promise<League> {
        this.logger.verbose("SportsScraper: getLeague")
        const leagueData = await this.jsonScraper.loadJson(parse(baseJSONUrl + "tournament_info.json?tournament=" + leagueId))

        const seasonsResponse = await this.jsonScraper.loadJson(parse(baseJSONUrl + "tournament_stat_seasons.json?tournament=" + leagueId))
        const seasons = seasonsResponse.map((seasonObj: any) => new Season(seasonObj["id"], seasonObj["name"]))

        const lastSeason = seasons[seasons.length - 1]

        const tableResponse = await this.jsonScraper.loadJson(parse(baseJSONUrl + "stat_table.json?id=" + lastSeason.id))
        const standings = tableResponse["commands"].map((standingRowObj: any) => {
            return new StandingRow(standingRowObj["place"],
                                    standingRowObj["group_name"] || "",
                                    new Team(standingRowObj["name"], []),
                                    standingRowObj["matches"],
                                    standingRowObj["wins"],
                                    standingRowObj["drawns"],
                                    standingRowObj["defeats"],
                                    standingRowObj["goals"],
                                    standingRowObj["conceded_goals"],
                                    standingRowObj["score"],
                                    standingRowObj["color"])
        })
        const roundsResponse = await this.jsonScraper.loadJson(parse(baseJSONUrl + "tournament_calendar.json?tournament=" + leagueId))
        const roundPromises: Promise<Round>[] = roundsResponse.map((r: any, i: number) => this.parseRound(r, i))
        const rounds = await Promise.all(roundPromises)

        const league = new League(leagueData["name"], lastSeason.name, rounds, standings)

        return league
    }

    async getTeamDefs(leagueId: number): Promise<SportsruObject[]> {
        this.logger.verbose("SportsScraper: getTeamDefs")

        const seasonsResponse = await this.jsonScraper.loadJson(parse(baseJSONUrl + "tournament_stat_seasons.json?tournament=" + leagueId))
        const seasons = seasonsResponse.map((seasonObj: any) => new Season(seasonObj["id"], seasonObj["name"]))

        const lastSeason = seasons[seasons.length - 1]

        const tableResponse = await this.jsonScraper.loadJson(parse(baseJSONUrl + "stat_table.json?id=" + lastSeason.id))
        const result = tableResponse["commands"].map((teamDefObj: any) => {
            return { name: teamDefObj["name"], id: teamDefObj["id"], tag_id: teamDefObj["tag_id"] }
        })

        return result
    }

    async getNews(tagId: number, number: number): Promise<News[]> {
        this.logger.verbose("SportsScraper: getNews")

        const url = `${baseJSONUrl}tag_lenta_with_filters.json?tag_id=${tagId}&count=${number}&available_content_types[]=news&main_only=1`
        this.logger.verbose("SportsScraper.url: " + url)
        const newsResponse = await this.jsonScraper.loadJson(parse(url))
        const news = newsResponse["feed"].map((newsObj: any) => new News(newsObj["title"], newsObj["link"]))

        return news
    }

    // MARK: Private

    private static tournamentNameToMatchType(matchTypeName: string): MatchType {
        let tournamentType = MatchType.Friendly
        if (matchTypeName == "Россия. Премьер-лига") {
            tournamentType = MatchType.League
        } else if (matchTypeName == "Лига Европы") {
            tournamentType = MatchType.EuropaLeague
        } else if (matchTypeName == "Россия. Олимп Кубок России") {
            tournamentType = MatchType.NationalCup
        } else if (matchTypeName == "Лига чемпионов") {
            tournamentType = MatchType.ChampionsLeague
        }
        return tournamentType
    }

    private async parseMatch(matchData: any): Promise<Match> {
        const id = matchData["id"] ?? 0
        const date = new Date(matchData["time"] * 1000)
        const notStarted = matchData["status_id"] == 1
        const isPostponed = matchData["status_id"] == 22
        const homePenalty = Number(matchData["command1"]["penalty"] || matchData["command1"]["penalty_win"])
        const awayPenalty = Number(matchData["command2"]["penalty"] || matchData["command2"]["penalty_win"])
        const match = new Match(
            id,
            SportsScraper.tournamentNameToMatchType(matchData["tournament_name"]),
            date,
            new Team(matchData["command1"]["name"], []),
            new Team(matchData["command2"]["name"], []),
            isPostponed,
            notStarted ? undefined : Number(matchData["command1"]["score"]),
            notStarted ? undefined : Number(matchData["command2"]["score"]),
            (notStarted || homePenalty == 0) ? undefined : homePenalty,
            (notStarted || awayPenalty == 0) ? undefined : awayPenalty
        )
        let currentMinute = undefined
        const diff = moment.duration(moment().diff(date)).asHours()
        if ((diff > 0) && (diff < 3)) {
            try {
                const arrange = await this.jsonScraper.loadJson(parse(baseUrl + "/stat/football/results/matches/" + matchData["id"] + "/arrange.json"))
                if (Number(arrange["status"]) != 4) {
                    currentMinute = Number(arrange["time"].split(":")[0])
                }
            } catch {}
        }
        match.currentMinute = currentMinute
        return match
    }

    private async parseRound(roundData: any, index: number): Promise<Round> {
        const roundMatchesData: any[] = roundData["matches"]
        const matches = await Promise.all(roundMatchesData.map((m: any) => this.parseMatch(m)))
        return new Round(index, roundData["stage_name"], matches)
    }
}

class Season {
    id: string
    name: string

    constructor(id: string, name: string) {
        this.id = id
        this.name = name
    }
}