import TelegramBot from 'node-telegram-bot-api'

import { BaseHandler } from "./baseHandler"

export default class BabichHandler extends BaseHandler {
    getCommandPattern(): RegExp {
        return /\/babich/
    }
    handleCommand(msg: TelegramBot.Message, _match: RegExpExecArray | null) {
        this.sendMessage("Сынок, играй хорошо, не груби сестре, слушайся родителей, уважай старших, будь хорошим мальчиком, но помни: МЫ ВСЕЙ СЕМЬЕЙ НЕНАВИДИМ ДИНАМО!", msg)
    }
}