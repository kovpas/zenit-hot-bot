import moment = require('moment')

import { Match } from '../models/match'
import { LastNextBaseHandler } from './lastNextBaseHandler'
import { Round } from '../models/league'

export default class NextHandler extends LastNextBaseHandler {

    getCommandPattern(): RegExp {
        return /\/next/
    }

    filterFunc(now: moment.Moment, match: Match) {
        return now.isBefore(match.date) && match.id != 1767036
    }
    
    sortFunc(matchA: Match, matchB: Match) {
        return moment(matchA.date).isAfter(matchB.date) ? 1 : -1
    }
    
    sortRoundFunc(roundA: Round, roundB: Round) {
        return roundA.pos > roundB.pos ? 1 : -1
    }
}