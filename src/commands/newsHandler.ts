import TelegramBot from 'node-telegram-bot-api'

import { BaseHandler } from "./baseHandler"
import { News } from '../models/news'
import * as htmlEntities from "html-entities"

export default class NewsHandler extends BaseHandler {

    getCommandPattern(): RegExp {
        return /\/news(.*)/
    }

    async handleCommand(msg: TelegramBot.Message, match: RegExpExecArray | null) {
        const chatId = msg.chat.id
        let teamName = "Зенит"
        let num = this.readArgument(match, 1, Number.NaN)
        const args = this.parseArguments(match)
        if (isNaN(num)) { 
            teamName = args.slice(1, args.length).join(" ")
            num = this.readArgument(match, args.length + 1, 5)
        } else {
            teamName = args.slice(2).join(" ")
        }
        if (teamName == "") {
            teamName = "Зенит"
        }

        const alias = this.resolveAlias(teamName).toLocaleLowerCase()
        const def = this.mappingDb.data.teams.find(item => item.name.toLocaleLowerCase() == alias) 
                    || this.mappingDb.data.leagues.find(item => item.name.toLocaleLowerCase() == alias)
        if (def == undefined) {
            this.sendMessage(`Я не знаю команду или лигу ${teamName}.`, msg)
            return
        }

        const tagId = def.tag_id
        const news = await this.sportsScraper.getNews(tagId, num)
        this.sendNews(news, msg)
    }

    private sendNews(news: News[], msg: TelegramBot.Message) {
        const message = news.map(news => ` - [${htmlEntities.decode(news.title)}](${news.url})`).join("\n\n")
        this.telegramBot.sendMessage(msg.chat.id, `Свежие новости! Молния!\n\n${message}`, {reply_to_message_id: msg.message_id, parse_mode: 'Markdown', disable_web_page_preview: true})
    }
}
