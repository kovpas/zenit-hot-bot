import moment = require('moment')

import fs from 'fs'

import TelegramBot from 'node-telegram-bot-api'
import { MatchOutcome, MatchTeamOutcome, MatchType } from '../models/match'

import { BaseHandler } from "./baseHandler"

const mayakCacheFileName = 'cache/mayak.json'
const initialData = {count: 515, date: new Date(1635523200000)}

export default class MayakHandler extends BaseHandler {
    getCommandPattern(): RegExp {
        return /\/mayak/
    }

    async handleCommand(msg: TelegramBot.Message, match: RegExpExecArray | null) {
        const mayakData = await this.reloadMayakData()
        const mayak = mayakData["count"]
        this.sendImage("resources/images/dynamo.png", msg, `Маяковский счетчик: ${3000 - mayak}`)
    }

    async reloadMayakData() {
        let mayakData = initialData
        try {
            mayakData = require("../" + mayakCacheFileName)
        } catch {}
        
        const alias = this.resolveAlias("Динамо").toLocaleLowerCase()
        const teamDef = this.mappingDb.data.teams.find(item => item.name.toLocaleLowerCase() == alias)
        if (teamDef != undefined) {
            const team = await this.sportsScraper.getTeam(teamDef.id)
            const officialMatches = team.matches.filter(match => match.matchType != MatchType.Friendly)
            const finishedMatches = officialMatches.filter(match => match.outcome() != MatchOutcome.Undefined)
            const lostMatches = finishedMatches.filter(match => match.outcomeForTeam(team) == MatchTeamOutcome.Lost)
            const lostSinceRefDate = lostMatches
                                        .filter(match => moment(match.date).isAfter(mayakData.date))
                                        .sort((matchA, matchB) => moment(matchA.date).isAfter(matchB.date) ? 1 : -1)
            
            if (lostSinceRefDate.length > 0) {
                mayakData = {count: mayakData.count + lostSinceRefDate.length, date: lostSinceRefDate[0].date}
            }
            fs.writeFileSync(mayakCacheFileName, JSON.stringify(mayakData))
        }
        return mayakData
    }
}
