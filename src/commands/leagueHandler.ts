import TelegramBot from 'node-telegram-bot-api'

import { BaseHandler } from "./baseHandler"
import { SportsruObject } from '../scrapers/sports';

export default class LeagueHandler extends BaseHandler {
    getCommandPattern(): RegExp {
        return /\/leagues? (init .+|search .+)/
    }
    
    handleCommand(msg: TelegramBot.Message, match: RegExpExecArray | null) {
        const chatId = msg.chat.id
        const args = this.parseArguments(match)
        if (args.length < 1) {
            this.sendMessage(`Правильный синтаксис команды: /leagues (init | search) league_name.`, msg)
            return
        }
        const command = args[1]
        const leftover = args.slice(2).join(" ")
        if (command == "init") {
            this.handleInitCommand(msg, leftover)
        } else if (command == "search") {
            this.handleSearchCommand(msg, leftover)
        }
    }

    private async handleInitCommand(msg: TelegramBot.Message, leagueAlias: string) {
        const leagueDef = this.leagueByAlias(leagueAlias)
        const teamDefs = await this.sportsScraper.getTeamDefs(leagueDef.id)
        
        await Promise.all(teamDefs.map(teamDef => this.upsertTeamDef(teamDef)))

        this.sendMessage(`В этой лиге есть следующие команды:\n${teamDefs.map(teamDef => {
            return teamDef.name + " (" + teamDef.id + ")"
        }).join("\n")}`, msg)
    }

    private async handleSearchCommand(msg: TelegramBot.Message, leagueAlias: string) {
        const leagueName = this.resolveAlias(leagueAlias).toLocaleLowerCase()
        const leagues = this.mappingDb.data.leagues
            .filter(item => item.name.toLocaleLowerCase().indexOf(leagueName) != -1)
        
        if (leagues === undefined || leagues.length == 0) {
            this.sendMessage("Лиги не найдены", msg)
            return
        }

        this.sendMessage(`Результаты поиска:\n${leagues.map(league => {
            return league.name + " (" + league.id + ")"
        }).join("\n")}`, msg)
    }

    private async upsertTeamDef(teamDef: SportsruObject) {
        const { id } = teamDef;
        const index = this.mappingDb.data.teams.findIndex(item => item.id == id);

        if (index > -1) {
            this.mappingDb.data.teams[index] = teamDef;
        } else {
            this.mappingDb.data.teams.push(teamDef);
        }

        this.mappingDb.write();
    }

}