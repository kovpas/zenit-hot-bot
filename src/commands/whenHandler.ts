import TelegramBot from 'node-telegram-bot-api'

import { BaseHandler } from "./baseHandler"
import moment = require('moment-timezone')
import { Team } from '../models/team'
import { Match } from '../models/match'

export default class WhenHandler extends BaseHandler {
    getCommandPattern(): RegExp {
        return /\/when/
    }

    async handleCommand(msg: TelegramBot.Message, _match: RegExpExecArray | null) {
        const teamId = 21
        const team = await this.sportsScraper.getTeam(teamId)

        this.sendMatchAnnounce(team, msg)
    }

    private sendMatchAnnounce(team: Team, msg: TelegramBot.Message) {
        const now = moment()
        const match = team.matches.filter((m: Match) => {
            return now.isBefore(m.date)
        }).sort((a: Match, b: Match) => {
            if (moment(a.date).isAfter(b.date)) { return 1 }
            return -1
        })[0]

        if (match === undefined) {
            this.sendMessage( "А матчей не будет, похоже. Может сезон окончен?", msg)
            return
        }

        const isHomeMatch = (match.homeTeam.name == team.name)
        const opponent = isHomeMatch ? match.awayTeam.name : match.homeTeam.name
        const date = moment(match.date)
        const format = "HH:mm, DD MMMM"
        const msk = date.tz('Europe/Moscow').format(format)
        const eet = date.tz('Europe/Helsinki').format(format)
        const cet = date.tz('Europe/Amsterdam').format(format)
        const est = date.tz('America/New_York').format(format)
        const message = `Играем ${date.fromNow()} против команды ${opponent} ${isHomeMatch ? "дома" : "на выезде"}.
\`\`\`
Москва, Петербург:   ${msk}
Йоэнсуу, Хельсинки:  ${eet}
Амстердам, Белград:  ${cet}
Бостон:              ${est}
\`\`\``
        this.telegramBot.sendMessage(msg.chat.id, message, {reply_to_message_id: msg.message_id, parse_mode: 'Markdown'})
    }
}