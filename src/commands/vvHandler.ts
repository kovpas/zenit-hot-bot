import TelegramBot from 'node-telegram-bot-api'

import { URL } from 'url'

import { BaseHandler } from "./baseHandler"
import { XMLElement } from 'libxmljs'

export default class VVHandler extends BaseHandler {
    private vvUrl = 'http://spartak.msk.ru/guest/viewforum.php?f=35'
    private guestUrl = 'http://spartak.msk.ru/guest/'
    private interestingRegex = "зенит|зинит|газовик|бомж|бамж|\sвар\s|пиздец|вилков|болот|п[ие]тер"

    getCommandPattern(): RegExp {
        return /\/vv( .*)?/
    }

    async handleCommand(msg: TelegramBot.Message, match: RegExpExecArray | null) {
        const chatId = msg.chat.id
        const args = this.parseArguments(match)
        this.logger.debug(`${args}`)
        var regex = this.interestingRegex

        if (args.length > 1) {
            regex = args.slice(1).join(" ")
        }

        const lastTopicPath = (await this.lastTopicPath()).replace(/^.\//, "")
        const allComments = await this.allComments(lastTopicPath)
        const interestingComments = allComments.filter(comment => RegExp(regex, "i").test(comment))
        if (interestingComments.length == 0) {
            this.sendMessage("Не пишут ничего интересного. Свиньи же, что с них взять...", msg)
            return
        }

        var comment = interestingComments[Math.floor(Math.random() * interestingComments.length)].trim()
        this.logger.verbose(`${comment}`)

        this.sendMessage(comment, msg)
    }

    private async lastTopicPath(): Promise<string> {
        const doc = await this.htmlScraper.rootNodeForPage(new URL(this.vvUrl))
        const lastTopic = doc.find('//div[@class="forumbg"]//a[@class="topictitle"]')[0] as XMLElement;
        const lastTopicLink = lastTopic.getAttribute('href')!.value()
        this.logger.verbose('Last topic path: ' + lastTopic)
        return lastTopicLink
    }

    private async allComments(url: string): Promise<string[]> {
        const doc = await this.htmlScraper.rootNodeForPage(new URL(this.guestUrl + "/" + url))
        const topics = doc.find('//div[@class="postbody postbody_w100"]')
        const allPosts = topics.map(topicNode => {
            var text = ""
            var quote = topicNode.get('.//blockquote') as XMLElement;
            if (quote != undefined) {
                try { (quote.get('.//cite')! as XMLElement).remove() } catch {}

                text += quote.find('.//div//text()')!.join("\n> ")

                if (text.trim() != "") { text = "> " + text; text += "\n\n" }
                quote.remove()
            }

            text += topicNode.find('.//div[@class="content"]//text()')!.join("\n")

            return text
        })
        return allPosts
    }
}
