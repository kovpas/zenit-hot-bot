import TelegramBot from 'node-telegram-bot-api'

import { BaseHandler } from "./baseHandler"
import { SportsruObject } from '../scrapers/sports';
import { MatchesImageGenerator } from '../image_generators/matches'
import { Team } from '../models/team';
import { MatchType } from '../models/match';

export default class HeadToHeadHandler extends BaseHandler {
    getCommandPattern(): RegExp {
        return /\/(headToHead|h2h|hh).*\s.*/
    }
    async handleCommand(msg: TelegramBot.Message, match: RegExpExecArray | null) {
        let args = this.parseArguments(match).slice(1)
        let leagueOnly = args[0] == "league"
        if (leagueOnly == true) {
            args = args.slice(1)
        }
        let teamA: SportsruObject | null = null
        let teamB: SportsruObject | null = null
        let num = Number(this.readArgument(match, leagueOnly ? 2 : 1, NaN))
        let index = 1
        if (isNaN(num)) { 
            num = 10 
        } else {
            args = args.slice(1)
        }
        while (teamA == null) {
            const nameA = args.slice(0, index).join(" ")
            const aliasA = this.resolveAlias(nameA).toLocaleLowerCase()
            teamA = this.mappingDb.data.teams.find(item => item.name.toLocaleLowerCase() == aliasA) || null
            if (index++ > args.length) { break }
        }
        if (teamA == null) {
            this.sendMessage(`Я не смог разобрать первую команду. Вообще ничего не понял.`, msg)
            return
        }
        const nameB = args.slice(index - 1).join(" ")
        const aliasB = this.resolveAlias(nameB).toLocaleLowerCase()
        teamB = this.mappingDb.data.teams.find(item => item.name.toLocaleLowerCase() == aliasB) || null
        if (teamB == null) {
            this.sendMessage(`Я не знаю команду ${nameB}.`, msg)
            return
        }

        const matches = await (await this.sportsScraper.getHeadToHead(teamA.id, teamB.id, num)).filter(match => {
            return leagueOnly ? match.matchType == MatchType.League : true
        })

        const imageGenerator = new MatchesImageGenerator()
        const matchesImage = await imageGenerator.generateImageBufferFromMatches(matches, this.replacementRules, new Team(teamA.name, []))
        this.sendImage(matchesImage, msg)
    }
}