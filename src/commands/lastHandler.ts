import moment = require('moment')

import { LastNextBaseHandler } from "./lastNextBaseHandler"
import { Match } from '../models/match'
import { Round } from '../models/league'

export default class LastHandler extends LastNextBaseHandler {
    getCommandPattern(): RegExp {
        return /\/last/
    }

    filterFunc(now: moment.Moment, match: Match) {
        return now.isAfter(match.date) && match.id != 1767209
    }
    
    sortFunc(matchA: Match, matchB: Match) {
        return moment(matchA.date).isBefore(matchB.date) ? 1 : -1
    }

    sortRoundFunc(roundA: Round, roundB: Round) {
        return roundA.pos < roundB.pos ? 1 : -1
    }
}