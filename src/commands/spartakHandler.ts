import TelegramBot from 'node-telegram-bot-api'

import { BaseHandler } from "./baseHandler"
const canvas = require('canvas')

export default class SpartakHandler extends BaseHandler {
    getCommandPattern(): RegExp {
        return /\/(?:m|myaso)(?:@.*)?$/
    }

    async handleCommand(msg: TelegramBot.Message, match: RegExpExecArray | null) {
        this.logger.verbose("SpartakHandler.handleCommand")

        this.sendImage("resources/images/spartak.jpg", msg)
    }
}