import TelegramBot from 'node-telegram-bot-api'

import { BaseHandler } from "./baseHandler"

export default class AliasHandler extends BaseHandler {
    getCommandPattern(): RegExp {
        return /\/alias (add .+=.+|remove .+|list$)/
    }

    handleCommand(msg: TelegramBot.Message, match: RegExpExecArray | null) {
        const chatId = msg.chat.id
        if ((match != undefined) && (match["input"] != undefined)) {
            const elements = match["input"].split(" ")
            const command = elements[1]
            const leftover = elements.slice(2).join(" ")
            if (command == "add") {
                this.handleAddCommand(msg, leftover)
            } else if (command == "remove") {
                this.handleRemoveCommand(msg, leftover)
            } else if (command == "list") {
                this.handleListCommand(msg, leftover)
            }
        }
    }

    private handleAddCommand(msg: TelegramBot.Message, params: string) {
        const elements = params.split("=")
        const alias = elements[0].trim()
        const original = elements[1].trim()
        const existingAlias = this.mappingDb.data.aliases
                                  .find(item => item.name == alias.toLocaleLowerCase())

        if (existingAlias != undefined) {
            this.sendMessage(`Ошибка: такой алиас уже есть. Команда /alias remove ${alias} удалит его.`, msg)
            return
        }
        
        this.mappingDb.data.aliases
            .push({ name: alias, sportsruName: original })

        this.mappingDb.write()
        this.sendMessage(`Хорошо, я записал. Отныне, ${alias} и ${original} для меня одно и то же.`, msg)
    }

    private handleRemoveCommand(msg: TelegramBot.Message, params: string) {
        const alias = params.trim()
        this.mappingDb.data.aliases = this.mappingDb.data.aliases
            .filter(item => item.name == alias.toLocaleLowerCase())

        this.mappingDb.write()

        this.sendMessage(`Алиас ${alias} удален.`, msg)
    }

    private handleListCommand(msg: TelegramBot.Message, _params: string) {
        const result = this.mappingDb.data.aliases.map(a => a.name + " = " + a.sportsruName)
        this.sendMessage(`Список всех алиасов:\n${result.join("\n")}`, msg)
    }
}