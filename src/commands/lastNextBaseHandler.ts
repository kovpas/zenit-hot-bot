import TelegramBot from 'node-telegram-bot-api'
import moment = require('moment')

import { BaseHandler } from "./baseHandler"
import { Team } from '../models/team'
import { MatchesImageGenerator } from '../image_generators/matches'
import { RoundsImageGenerator } from '../image_generators/rounds'
import { Match } from '../models/match'
import { League, Round } from '../models/league'

export abstract class LastNextBaseHandler extends BaseHandler {
    async handleCommand(msg: TelegramBot.Message, match: RegExpExecArray | null) {
        const chatId = msg.chat.id
        let teamName = "Зенит"
        let num = this.readArgument(match, 1, Number.NaN)
        const args = this.parseArguments(match)
        if (isNaN(num)) { 
            teamName = args.slice(1, args.length).join(" ")
            num = this.readArgument(match, args.length + 1, 5)
        } else {
            teamName = args.slice(2).join(" ")
        }
        if (teamName == "") {
            teamName = "Зенит"
        }

        const alias = this.resolveAlias(teamName).toLocaleLowerCase()
        const teamDef = this.mappingDb.data.teams.find(item => item.name.toLocaleLowerCase() == alias)
        if (teamDef != undefined) {
            const team = await this.sportsScraper.getTeam(teamDef.id)
            this.sendTeamMatchesAnnounce(team, num, msg)
            return
        }

        const leagueDef = this.mappingDb.data.leagues.find(item => item.name.toLocaleLowerCase() == alias)
        if (leagueDef != undefined) {
            const league = await this.sportsScraper.getLeague(leagueDef.id)
            num = this.readArgument(match, 1, Number.NaN)
            if (isNaN(num)) { num = this.readArgument(match, args.length + 1, 1) }
            this.sendLeagueMatchesAnnounce(league, num, msg)
            return
        }

        this.sendMessage(`Я не знаю команду или лигу ${teamName}.`, msg)
    }

    abstract filterFunc(now: moment.Moment, match: Match): Boolean
    abstract sortFunc(matchA: Match, matchB: Match): number
    abstract sortRoundFunc(roundA: Round, roundB: Round): number

    private async sendTeamMatchesAnnounce(team: Team, numberOfMatches: number, msg: TelegramBot.Message) {
        const now = moment()
        const matches = team.matches
            .filter((m: Match) => this.filterFunc(now, m))
            .sort(this.sortFunc)
            .slice(0, numberOfMatches)

        const imageGenerator = new MatchesImageGenerator()
        const matchesImage = await imageGenerator.generateImageBufferFromMatches(matches, this.replacementRules, team)
        this.sendImage(matchesImage, msg)
    }

    private async sendLeagueMatchesAnnounce(league: League, numberOfRounds: number, msg: TelegramBot.Message) {
        const now = moment()
        const rounds = league.rounds
            .map((r: Round) => new Round(r.pos, r.name, r.matches.filter((m: Match) => this.filterFunc(now, m))))
            .filter((r: Round) => r.matches.length != 0)
            .sort(this.sortRoundFunc)
            .slice(0, numberOfRounds)

        const imageGenerator = new RoundsImageGenerator()
        const roundsImage = await imageGenerator.generateImageFromRounds(rounds, league, this.replacementRules)
        this.sendImage(roundsImage, msg)
    }
}