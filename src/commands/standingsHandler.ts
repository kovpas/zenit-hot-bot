import TelegramBot from 'node-telegram-bot-api'

import { BaseHandler } from "./baseHandler"
import { LeagueImageGenerator } from '../image_generators/league'

export default class StandingsHandler extends BaseHandler {
    getCommandPattern(): RegExp {
        return /\/(?:s|standings)(.*)/
    }

    async handleCommand(msg: TelegramBot.Message, match: RegExpExecArray | null) {
        this.logger.verbose("StandingsHandler.handleCommand")
        let leagueAlias = "rfpl"
        const args = this.parseArguments(match)
        this.logger.verbose(`${args}`)
        if (args.length > 1) {
            leagueAlias = args.slice(1).join(" ")
        }
        try {
            const leagueDef = leagueAlias == "myaso" ? this.leagueByAlias("rfpl") : this.leagueByAlias(leagueAlias)
            this.logger.verbose("StandingsHandler.handleCommand. Found league, sportsruId: " + leagueDef.id)
            const league = await this.sportsScraper.getLeague(leagueDef.id) // TODO: cache?
            this.logger.debug("StandingsHandler.handleCommand. Got the league object: %j", league)
            const imageGenerator = new LeagueImageGenerator()
            const highlightTeamNames = [leagueAlias == "myaso" ? "Спартак" : "Зенит", "Россия"]
            if (leagueAlias == "myaso") {
                const myaso = league.standings.filter(row => row.team.name == "Спартак")
                league.standings = myaso.concat(league.standings.filter(row => row.team.name != "Спартак"))
            }
            const leagueImage = await imageGenerator.generateImageFromLeague(league, this.replacementRules, highlightTeamNames)
            this.logger.verbose("StandingsHandler.handleCommand. Generated image.")
            
            this.sendImage(leagueImage, msg)
        } catch {
            this.sendMessage(`Я не знаю лигу ${leagueAlias}.`, msg)
        }
    }
}