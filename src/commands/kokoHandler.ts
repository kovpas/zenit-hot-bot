import TelegramBot from 'node-telegram-bot-api'
import moment from 'moment'

import { BaseHandler } from "./baseHandler"

export default class KokoHandler extends BaseHandler {
    getCommandPattern(): RegExp {
        return /\/koko/
    }
    handleCommand(msg: TelegramBot.Message, _match: RegExpExecArray | null) {
        const kokoBackDate = moment("09:00, 17 сентября 2019", "HH:mm, DD MMM YYYY")
        const kokoBack = kokoBackDate.toNow(true)
        const message = [`Не забудьте, что Кокорин вернулся ${kokoBack} назад.`, 
                         `Кокорин не лепит шахматы из хлеба уже ${kokoBack}.`,
                         `Кокорин не жиреет на казенных харчах уже ${kokoBack}.`,
                         `Бесплатная еда у Кокорина закончилась ${kokoBack} назад.`,
                         `Мы смеемся потому, что Кокорин вышел ${kokoBack} назад.`,
                         `Все коробочки склеили. Закончили ${kokoBack} назад.`,
                         `Кокорин не следит за базаром уже ${kokoBack}.`,
                         `Кокорин спокойно поднимает мыло в душе уже ${kokoBack}.`]
        const randomMsg = message[Math.floor(Math.random() * message.length)]
        this.sendMessage(randomMsg.trim(), msg)
    }
}
