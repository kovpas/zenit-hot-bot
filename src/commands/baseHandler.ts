import TelegramBot from 'node-telegram-bot-api'
import winston from "winston"
import { Low } from 'lowdb'

import { SportsScraping, SportsruSchema, SportsruObject } from '../scrapers/sports'
import { HTMLScraper } from '../scrapers/html'

export default interface CommandHandling {
    handleCommand(msg: TelegramBot.Message, _match: RegExpExecArray | null): void
    getCommandPattern(): RegExp
}

export abstract class BaseHandler implements CommandHandling {
    protected logger: winston.Logger
    protected telegramBot: TelegramBot
    protected mappingDb: Low<SportsruSchema>
    protected sportsScraper: SportsScraping
    protected htmlScraper: HTMLScraper
    protected replacementRules = require("../../config/teams.json")["replacementRules"]

    constructor(logger: winston.Logger, telegramBot: TelegramBot, sportsScraper: SportsScraping, htmlScraper: HTMLScraper, mappingDb: Low<SportsruSchema>) {
        this.htmlScraper = htmlScraper
        this.logger = logger
        this.telegramBot = telegramBot
        this.mappingDb = mappingDb
        this.sportsScraper = sportsScraper
    }

    abstract handleCommand(msg: TelegramBot.Message, _match: RegExpExecArray | null): void
    abstract getCommandPattern(): RegExp

    protected sendMessage(message: string, msg: TelegramBot.Message) {
        this.telegramBot.sendMessage(msg.chat.id, message, {reply_to_message_id: msg.message_id})
    }

    protected sendImage(path: string | Buffer, msg: TelegramBot.Message, caption?: string) {
        if (typeof(path) == 'string') {
            this.logger.verbose(`BaseHandler.sendImage sending "${path}" with caption "${caption}" to ${msg.chat.id}`)
        }
        this.telegramBot.sendPhoto(msg.chat.id, path, {reply_to_message_id: msg.message_id, caption: caption}).then((msg: TelegramBot.Message) => {
            this.logger.debug("BaseHandler.sendImage telegram bot response: %j", msg)
        })
    }

    protected resolveAlias(alias: string): string {
        const aliasStorage = this.mappingDb.data.aliases
        const aliasLowCase = alias.toLocaleLowerCase()
        const aliasObj = aliasStorage.find(item => item.name.toLocaleLowerCase() == aliasLowCase)
        if (aliasObj !== undefined) {
            this.logger.verbose("BaseHandler.resolveAlias. Alias: `" + aliasObj.name + "` resolved to: `" + aliasObj.sportsruName + "`")
            return aliasObj.sportsruName
        }

        return alias
    }

    protected leagueByAlias(alias: string): SportsruObject {
        const leaguesStorage = this.mappingDb.data.leagues
        const leagueName = this.resolveAlias(alias).toLocaleLowerCase()
        const league = leaguesStorage.find(item => item.name.toLocaleLowerCase() == leagueName)
        if (league === undefined) {
            const error = Error(`BaseHandler.leagueByAlias. league with name ${leagueName} was not found`)
            this.logger.error(error)
            throw error
        }

        return league
    }

    protected readArgument<T extends number | string>(match: RegExpExecArray | null, index: number, defaultValue: T): T {
        const args = this.parseArguments(match)
        if (args.length <= index) {
            this.logger.warn(`BaseHandler.readArgument. Attempting to get a parameter with index ${index}, from the list: ${args}. Returning default vaule: ${defaultValue}`)
            return defaultValue
        }

        this.logger.verbose(`BaseHandler.readArgument. Retruning ${args[index]}`)
        return args[index] as T
    }

    protected parseArguments(match: RegExpExecArray | null): string[] {
        if ((match == undefined) || (match["input"] == undefined)) {
            this.logger.warn("BaseHandler.arguments. Either match or match.input is undefined. Returning empty array")
            return []
        }
        
        const elements = match["input"].split(' ').map(s => s.trim())

        this.logger.verbose(`BaseHandler.arguments. Retruning ${elements}`)
        return elements
    }
}