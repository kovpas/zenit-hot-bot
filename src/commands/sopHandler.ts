import TelegramBot from 'node-telegram-bot-api'

import { BaseHandler } from "./baseHandler"

export const sopCacheFileName = 'cache/sop.json'

export default class SopHandler extends BaseHandler {
    getCommandPattern(): RegExp {
        return /\/sop/
    }

    handleCommand(msg: TelegramBot.Message, _match: RegExpExecArray | null) {
        try {
            const lastCachedSopcastMessage = require("../" + sopCacheFileName)
            this.sendMessage(lastCachedSopcastMessage.text!, msg)
        } catch {}
    }
}