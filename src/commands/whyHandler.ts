import TelegramBot from 'node-telegram-bot-api'
import moment = require('moment')

import { BaseHandler } from "./baseHandler"

export default class WhyHandler extends BaseHandler {
    getCommandPattern(): RegExp {
        return /\/why/
    }
    handleCommand(msg: TelegramBot.Message, _match: RegExpExecArray | null) {
        const chatId = msg.chat.id
        const mancEnd = moment("11:51, 13 мая 2018", "HH:mm, DD MMM YYYY")
        const notCoach = mancEnd.toNow(true)
        const intro = ["К слову,", "В это сложно поверить, но", "Мы все пляшем потому что", "Шутка ли,", ""]
        const name = ["Роберто Манчини", "Манчини"]
        const randomIntro = intro[Math.floor(Math.random() * intro.length)]
        const randomName = name[Math.floor(Math.random() * name.length)]
        this.sendMessage(`${randomIntro} ${randomName} не тренер Зенита уже ${notCoach}`.trim(), msg)
    }
}