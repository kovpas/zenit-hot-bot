#!/usr/bin/env ts-node
process.env["NTBA_FIX_319"] = "1"
import config from '../config.json'

const dbName = config["db_name"] || "sportsru"
const token = config["bot_token"]

import { HTTPClient, HTTPClientImpl } from './scrapers/httpclient.js'
import { HTMLScraper } from './scrapers/html.js'
import { JSONScraper } from './scrapers/json.js'
import { SportsScraper, SportsruSchema } from './scrapers/sports.js'
import { ZenitHotBot } from "./zenithotbot.js"

import type { Logger } from 'winston'
import { createLogger, format, transports } from 'winston'
import moment from 'moment'
import 'moment/locale/ru'
import { JSONPreset } from 'lowdb/node'

function createBotLogger(): Logger {
    const logger = createLogger({
        level: config["logging_level"] || 'error',
        format: format.prettyPrint(),
        transports: [
            new transports.Console()
        ]
    })
    
    return logger    
}

function createHtmlScraper(httpClient: HTTPClient, logger: Logger): HTMLScraper {
    logger.verbose("index.createHtmlScraper")

    const scraper = new HTMLScraper(logger, httpClient, 'utf8')
    return scraper
}

function createJsonScraper(httpClient: HTTPClient, logger: Logger): JSONScraper {
    logger.verbose("index.createJsonScraper")

    const scraper = new JSONScraper(logger, httpClient, 'utf8')
    return scraper
}

function createSportsScraper(jsonScraper: JSONScraper, htmlScraper: HTMLScraper, logger: Logger): SportsScraper {
    logger.verbose("index.createSportsScraper")
    
    const scraper = new SportsScraper(logger, htmlScraper, jsonScraper)
    return scraper
}

(async () => {
    moment.locale('ru')
    const logger = createBotLogger()
    const httpClient = new HTTPClientImpl(logger)

    const htmlScraper = createHtmlScraper(httpClient, logger)
    const jsonScraper = createJsonScraper(httpClient, logger)
    const sportsScraper = createSportsScraper(jsonScraper, htmlScraper, logger)
    const initialData = await import('../config/initial_data.json')
    const db = await JSONPreset<SportsruSchema>(dbName + ".json", initialData)
    const zenitHotBot = new ZenitHotBot(logger, token, sportsScraper, htmlScraper, db)
    
    zenitHotBot.activate()
})()
