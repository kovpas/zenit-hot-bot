import { Team } from './team'

export enum MatchType {
    NationalCup = "National Cup",
    League = "League",
    EuropaLeague = "Europa League",
    ChampionsLeague = "Champions League",
    Friendly = "Friendly"
}

export enum MatchOutcome {
    Home,
    Away,
    Draw,
    Postponed,
    Undefined
}

export enum MatchTeamOutcome {
    Won,
    Lost,
    Draw,
    Postponed,
    Undefined
}

export class Match {
    id: number
    matchType: MatchType
    date: Date
    homeTeam: Team
    awayTeam: Team
    homeScore?: number
    awayScore?: number
    penaltyHome?: number
    penaltyAway?: number
    currentMinute?: number
    isPostponed: boolean

    constructor(id: number, matchType: MatchType, date: Date, homeTeam: Team, awayTeam: Team, isPostponed: boolean = false, homeScore?: number, awayScore?: number, penaltyHome?: number, penaltyAway?: number, currentMinute?: number) {
        this.id = id
        this.matchType = matchType
        this.date = date
        this.homeTeam = homeTeam
        this.awayTeam = awayTeam
        this.homeScore = homeScore
        this.awayScore = awayScore
        this.penaltyHome = penaltyHome
        this.penaltyAway = penaltyAway
        this.currentMinute = currentMinute
        this.isPostponed = isPostponed
    }

    outcome(): MatchOutcome {
        if (this.penaltyHome != undefined && this.penaltyAway != undefined) { // assuming a draw in the match
            return (this.penaltyHome > this.penaltyAway) ? MatchOutcome.Home : MatchOutcome.Away
        }
        if (this.homeScore != undefined && this.awayScore != undefined) {
            if (this.homeScore > this.awayScore) {
                return MatchOutcome.Home
            } else if (this.homeScore < this.awayScore) {
                return MatchOutcome.Away
            } 
            return MatchOutcome.Draw
        }
        return MatchOutcome.Undefined
    }

    byPenalty(): boolean {
        return (this.penaltyHome != undefined && this.penaltyAway != undefined)
    }

    outcomeForTeam(team: Team): MatchTeamOutcome {
        if (!(this.homeTeam.name == team.name) && !(this.awayTeam.name == team.name)) {
            return MatchTeamOutcome.Undefined
        }

        const outcome = this.outcome()
        
        if (outcome == MatchOutcome.Draw) {
            return MatchTeamOutcome.Draw
        }

        if ((this.homeTeam.name == team.name && outcome == MatchOutcome.Home)
           || (this.awayTeam.name == team.name && outcome == MatchOutcome.Away)) {
               return MatchTeamOutcome.Won
        }

        if ((this.homeTeam.name == team.name && outcome == MatchOutcome.Away)
           || (this.awayTeam.name == team.name && outcome == MatchOutcome.Home)) {
               return MatchTeamOutcome.Lost
        }

        return MatchTeamOutcome.Undefined
    }
}