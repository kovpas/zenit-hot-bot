import { Match } from "./match";

export class Team {
    name: string
    matches: Match[]

    constructor(name: string, matches: Match[]) {
        this.name = name
        this.matches = matches
    }
}