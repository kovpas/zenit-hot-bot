import { Match } from './match'
import { Team } from './team'

export class Round {
    pos: number
    name: string
    matches: Match[]

    constructor(pos: number, name: string, matches: Match[]) {
        this.pos = pos
        this.name = name
        this.matches = matches
    }
}

export class StandingRow {
    team: Team
    place: number
    group: string
    matches: number
    won: number
    drawn: number
    lost: number
    goalsFor: number
    goalsAgainst: number
    points: number
    color: number

    constructor(place: number, group: string, team: Team, matches: number, won: number, drawn: number, lost: number, goalsFor: number, goalsAgainst: number, points: number, color?: number) {
        this.place = place
        this.group = group
        this.team = team
        this.matches = matches
        this.won = won
        this.drawn = drawn
        this.lost = lost
        this.goalsFor = goalsFor
        this.goalsAgainst = goalsAgainst
        this.points = points
        this.color = color || 0
    }
}

export class League {
    name: string
    year: string
    rounds: Round[]
    standings: StandingRow[]

    constructor(name: string, year: string, rounds: Round[], standings: StandingRow[]) {
        this.name = name
        this.year = year
        this.rounds = rounds
        this.standings = standings
    }
}