const canvas = require('canvas')

import { Match, MatchOutcome, MatchType } from '../models/match'
import { Team } from '../models/team'
import moment = require('moment-timezone')

export class MatchesImageGenerator {
    private multiplier = 2
    private width = 400 * this.multiplier
    private rowHeight = 30 * this.multiplier
    private separatorWidth = 1 * this.multiplier
    private fontSize = 13 * this.multiplier

    private separatorColor = (212).toString(16) + (212).toString(16) + (212).toString(16)
    private bgColor = (249).toString(16) + (249).toString(16) + (247).toString(16)

    private teamNameColor = '0' + (0).toString(16) + (100).toString(16) + (150).toString(16)
    private teamStatsColor = (34).toString(16) + (34).toString(16) + (34).toString(16)
    private winColor = '0' + (0).toString(16) + (110).toString(16) + (30).toString(16)
    private drawColor = (210).toString(16) + (110).toString(16) + '0' + (0).toString(16)
    private loseColor = (155).toString(16) + '0' + (0).toString(16) + (35).toString(16)
    private undefColor = (231).toString(16) + (231).toString(16) + (231).toString(16)

    async generateImageBufferFromMatches(matches: Match[], replacementRules: {[key: string]: string}, team?: Team): Promise<Buffer> {
        const canvas = await this.generateImageFromMatches(matches, replacementRules, team)
        return canvas.toBuffer()
    }
    async generateImageFromMatches(matches: Match[], replacementRules: {[key: string]: string}, team?: Team): Promise<any> {
        canvas.registerFont('resources/fonts/Roboto-Regular.ttf', {family: 'Roboto'})
        canvas.registerFont('resources/fonts/Roboto-Bold.ttf', {family: 'Roboto', weight: "700"})
        canvas.registerFont('resources/fonts/Roboto-Black.ttf', {family: 'Roboto', weight: "900"})

        canvas.registerFont('resources/fonts/RobotoMono-Regular.ttf', {family: 'Roboto Mono'})
        canvas.registerFont('resources/fonts/RobotoMono-Bold.ttf', {family: 'Roboto Mono', weight: "700"})

        const rfplImage = await canvas.loadImage(`resources/images/rfpl.png`)
        const rcupImage = await canvas.loadImage(`resources/images/rcup.png`)
        const elImage = await canvas.loadImage(`resources/images/el.png`)
        const clImage = await canvas.loadImage(`resources/images/cl.png`)

        const images = {"rfpl": rfplImage, "rcup": rcupImage, "el": elImage, "cl": clImage}

        const config = this.generateImageConfig(matches, team)
        const maxWidth = Math.max(...config.map(conf => conf.resultWidth))

        const background = this.generateBackground(matches, config, images)
        const ctx = background.getContext('2d')

        ctx.font = this.fontSize + "px Roboto"
        matches.forEach((match: Match, index: number) => {
            const textY = (index + 1) * this.rowHeight - this.rowHeight / 3.25

            ctx.fillStyle = '#' + this.teamNameColor
            ctx.font = this.fontSize + "px Roboto"
            ctx.textAlign = 'end'
            ctx.fillText(this.generateTeamName(match.homeTeam.name, replacementRules), (this.width - maxWidth) / 2 - 20, textY)
            ctx.textAlign = 'start'
            ctx.fillText(this.generateTeamName(match.awayTeam.name, replacementRules), (this.width + maxWidth) / 2 + 20, textY)
            ctx.font = this.fontSize + "px Roboto Mono"
            ctx.textAlign = 'center'
            ctx.fillStyle = '#' + this.bgColor
            ctx.textBaseline = 'hanging'
            if (match.isPostponed || (match.outcome() == MatchOutcome.Undefined)) {
                ctx.fillStyle = '#' + this.teamStatsColor
                ctx.font = (this.fontSize - 3 * this.multiplier) + "px Roboto"
                ctx.fillText(match.isPostponed ? "Пере-\nнесен" : this.generateMatchDate(match), this.width / 2, index * this.rowHeight + 7)
            } else {
                ctx.font = "700 " + this.fontSize + "px Roboto Mono"
                ctx.fillText(this.generateMatchScore(match), this.width / 2, index * this.rowHeight + 7)
                ctx.font = (this.fontSize - 7 * this.multiplier) + "px Roboto"
                ctx.fillText(this.generateShortMatchDate(match), this.width / 2, (index + 1) * this.rowHeight - 18)
            }
            ctx.font = this.fontSize + "px Roboto Mono"
            ctx.textBaseline = 'alphabetic'

            if (match.currentMinute != undefined) {
                ctx.textBaseline = 'hanging'
                ctx.fillStyle = '#' + this.loseColor
                ctx.font = (this.fontSize - 5) + "px Roboto"
                ctx.textAlign = 'end'
                ctx.fillText(match.currentMinute + "'", this.width - 7 * this.multiplier, index * this.rowHeight + 7)
                ctx.textBaseline = 'alphabetic'
            }
        })

        return background
    }

    private generateBackground(matches: Match[], rows: RowConfig[], leagueImages: {[key: string]: any}): any {
        const background = canvas.createCanvas(this.width,
                                        this.rowHeight * matches.length + this.separatorWidth)
        const ctx = background.getContext('2d')

        ctx.fillStyle = '#' + this.bgColor
        ctx.fillRect(0, 0, this.width,
                        this.rowHeight * matches.length + this.separatorWidth)

        // vertical grid
        const maxWidth = Math.max(...rows.map(conf => conf.resultWidth))

        ctx.fillStyle = '#' + this.separatorColor
        ctx.fillRect(0, 0, this.width, this.separatorWidth);

        // // teams grid
        rows.forEach((value: RowConfig, index: number) => {
            if (leagueImages[value.pic]) {
                const size = this.rowHeight - 12 * this.multiplier
                const offset = 7 * this.multiplier
                ctx.drawImage(leagueImages[value.pic], offset, index * this.rowHeight + offset, size, size)
            }
            ctx.fillStyle = "#" + value.rowColor
            ctx.fillRect((this.width - maxWidth) / 2, index * this.rowHeight + this.separatorWidth,
                         maxWidth, this.rowHeight)
            ctx.fillStyle = '#' + this.separatorColor
            ctx.fillRect(0, (index + 1) * this.rowHeight, this.width, this.separatorWidth);
        })

        return background
    }

    private generateTeamName(teamName: string, replacementRules: {[key: string]: string}): string {
        let name = teamName
        Object.entries(replacementRules).forEach(([key, value]) => {
            name = name.replace(new RegExp(key, 'g'), value)
        })
        return name
    }

    private generateImageConfig(matches: Match[], team?: Team): RowConfig[] {
        const tmp = canvas.createCanvas()
        const ctx = tmp.getContext('2d')

        return matches.map((match: Match) => {
            let color = this.undefColor
            if (!match.isPostponed) {
                const outcome = match.outcome()
                let teamHome = true
                if (team != undefined) {
                    teamHome = match.homeTeam.name == team.name
                }
                if (outcome == MatchOutcome.Home) {
                    color = teamHome ? this.winColor : this.loseColor
                } else if (outcome == MatchOutcome.Away) {
                    color = teamHome ? this.loseColor : this.winColor
                } else if (outcome == MatchOutcome.Draw) {
                    color = this.drawColor
                }
            }
            ctx.font = this.fontSize + "px Roboto Mono"
            let width = Math.ceil(ctx.measureText(this.generateMatchScore(match)).width)
            if (match.outcome() == MatchOutcome.Undefined || match.isPostponed) {
                ctx.font = (this.fontSize - 5) + "px Roboto"
                width = Math.ceil(ctx.measureText(this.generateMatchDate(match)).width)
            }

            let pic = ""
            if (match.matchType == MatchType.NationalCup) {
                pic = "rcup"
            } else if (match.matchType == MatchType.EuropaLeague) {
                pic = "el"
            } else if (match.matchType == MatchType.ChampionsLeague) {
                pic = "cl"
            } else if (match.matchType == MatchType.League) {
                pic = "rfpl"
            }

            return new RowConfig(color, width + 20 * this.multiplier, pic)
        })
    }

    private generateMatchScore(match: Match): string {
        return (match.byPenalty() ? "п " : "" ) + (match.homeScore === undefined ? "-" : match.homeScore) + ":" + (match.awayScore === undefined ? "-" : match.awayScore) + (match.byPenalty() ? "  " : "" )
    }
    private generateMatchDate(match: Match): string {
        const date = moment(match.date)

        return date.format("DD.MM") + "\n" + (date.tz('Europe/Moscow').format("HH:mm"))
    }
    private generateShortMatchDate(match: Match): string {
        const date = moment(match.date)

        return date.format("DD.MM.YYYY")
    }
}

class RowConfig {
    rowColor: string
    resultWidth: number
    pic: string

    constructor(rowColor: string, resultWidth: number, pic: string) {
        this.rowColor = rowColor
        this.resultWidth = resultWidth
        this.pic = pic
    }
}
