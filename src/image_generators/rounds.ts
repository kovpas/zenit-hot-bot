import { Promise } from 'core-js'
const canvas = require('canvas')

import { Round, League } from '../models/league'
import { MatchesImageGenerator } from './matches'

export class RoundsImageGenerator {
    private matchesImageGenerator = new MatchesImageGenerator()

    private multiplier = 2
    private width = 400 * this.multiplier
    private separatorWidth = 1 * this.multiplier
    private fontSize = 13 * this.multiplier
    private cellWidth = 32 * this.multiplier
    private titleHeight = 30 * this.multiplier

    private roundTitleColor = '0' + (0).toString(16) + (170).toString(16) + (50).toString(16)

    async generateImageFromRounds(rounds: Round[], league: League, replacementRules: {[key: string]: string}): Promise<Buffer> {
        canvas.registerFont('resources/fonts/Roboto-Regular.ttf', {family: 'Roboto'})
        canvas.registerFont('resources/fonts/Roboto-Bold.ttf', {family: 'Roboto', weight: "700"})
        canvas.registerFont('resources/fonts/Roboto-Black.ttf', {family: 'Roboto', weight: "900"})

        canvas.registerFont('resources/fonts/RobotoMono-Regular.ttf', {family: 'Roboto Mono'})
        canvas.registerFont('resources/fonts/RobotoMono-Bold.ttf', {family: 'Roboto Mono', weight: "700"})

        const columns = 1
        const allRoundImages: any[] = await Promise.all(rounds.map(async (r: Round) => {
            const titleHeight = r.name == "" ? 0 : this.titleHeight
            const roundImage = await this.matchesImageGenerator.generateImageFromMatches(r.matches, replacementRules)
            const roundCanvas = canvas.createCanvas(roundImage.width, roundImage.height + this.separatorWidth + titleHeight)
            const ctx = roundCanvas.getContext('2d')

            ctx.font = (this.fontSize + 2) + "px Roboto"
            ctx.fillStyle = '#' + this.roundTitleColor
            ctx.textAlign = 'start' 
            ctx.textBaseline = 'middle'

            ctx.fillText(r.name, this.cellWidth / 4, titleHeight / 2 + 6)
            ctx.drawImage(roundImage, 0, this.separatorWidth + titleHeight)

            return roundCanvas
        }))
        let heights = [allRoundImages.reduce((result, image) => result + image.height, 0)]

        const background = canvas.createCanvas(this.width * columns + 20 * (columns - 1), Math.max.apply(null, heights))
        const ctx = background.getContext('2d')
        heights = new Array(columns).fill(0)
        allRoundImages.forEach((roundImage, index) => {
            const col = 0
            ctx.drawImage(roundImage, (this.width + 20) * col, heights[col])
            heights[col] += roundImage.height
        });

        return background.toBuffer()
    }
}
