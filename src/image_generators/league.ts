const canvas = require('canvas')

import { League, StandingRow } from '../models/league'
import { Team } from '../models/team';

type GroupStandings = {[key: string]: StandingRow[]}

export class LeagueImageGenerator {
    private colorConfig = require("../../config/colors.json")
    private multiplier = 2
    private rowHeight = 26 * this.multiplier
    private cellWidth = 32 * this.multiplier
    private width = 200 * this.multiplier + this.cellWidth * 8
    private titleHeight = 30 * this.multiplier
    private separatorWidth = 1 * this.multiplier
    private fontSize = 13 * this.multiplier
    private flagWidth = 16 * this.multiplier
    private flagHeight = 12 * this.multiplier

    private separatorColor = (212).toString(16) + (212).toString(16) + (212).toString(16)
    private bgColor = (249).toString(16) + (249).toString(16) + (247).toString(16)
    private bgAlterColor = (240).toString(16) + (240).toString(16) + (240).toString(16)

    private groupNameColor = '0' + (0).toString(16) + (170).toString(16) + (50).toString(16)
    private teamNameColor = '0' + (0).toString(16) + (100).toString(16) + (150).toString(16)
    private highlightedTeamColor = (200).toString(16) + (240).toString(16) + (249).toString(16)
    private teamStatsColor = (34).toString(16) + (34).toString(16) + (34).toString(16)

    async generateImageFromLeague(league: League, replacementRules: {[key: string]: string}, highlightTeamNames?: string[]): Promise<Buffer> {
        canvas.registerFont('resources/fonts/Roboto-Regular.ttf', {family: 'Roboto'})
        canvas.registerFont('resources/fonts/Roboto-Bold.ttf', {family: 'Roboto', weight: "700"})
        canvas.registerFont('resources/fonts/Roboto-Black.ttf', {family: 'Roboto', weight: "900"})
        const groups = normalizeArray(league.standings, "group")
        const background = this.generateLeague(groups, replacementRules, highlightTeamNames)

        return background.toBuffer()
    }

    private generateLeague(groups: GroupStandings, replacementRules: {[key: string]: string}, highlightTeamNames?: string[]): any {
        const groupImages = Object.keys(groups).map(key => this.generateGroupImage(key, groups[key], replacementRules, highlightTeamNames))
        const totalHeight = groupImages.reduce((acc, val) => acc + val.height, 0)

        const columns = (totalHeight / this.width > 2.5) ? 2 : 1
        let heights = new Array(columns).fill(0)
        console.log(totalHeight, this.width)
        groupImages.forEach((groupImage, index) => heights[index % columns] += groupImage.height)

        const background = canvas.createCanvas(this.width * columns + 20 * (columns - 1), Math.max.apply(null, heights))
        const ctx = background.getContext('2d')

        // Combine groups together
        heights = new Array(columns).fill(0)
        groupImages.forEach((groupImage, index) => {
            const col = index % columns
            ctx.drawImage(groupImage, (this.width + 20) * col, heights[col])
            heights[col] += groupImage.height
        });

        return background
    }

    private generateGroupImage(title: string, group: StandingRow[], replacementRules: {[key: string]: string}, highlightTeamNames: string[] = []): any {
        const titleHeight = title == "" ? 0 : this.titleHeight
        const groupImage = canvas.createCanvas(this.width,
            this.rowHeight * (group.length + 1) + this.separatorWidth + titleHeight)
        const ctx = groupImage.getContext('2d')

        ctx.fillStyle = '#' + this.bgColor
        ctx.fillRect(0, 0, this.width,
                        this.rowHeight * (group.length + 1) + this.separatorWidth)

        // title
        if (titleHeight > 0) {
            ctx.font = (this.fontSize + 2) + "px Roboto"
            ctx.fillStyle = '#' + this.groupNameColor
            ctx.textAlign = 'start'
            ctx.textBaseline = 'middle'
            ctx.fillText(title, this.cellWidth / 4, titleHeight / 2 + 6)
        }

        // teams grid
        const highlights = highlightTeamNames.map(i => i.toLocaleLowerCase())
        group.forEach((value: StandingRow, index: number) => {
            ctx.fillStyle = '#' + this.separatorColor
            ctx.fillRect(0, titleHeight + (index + 1) * this.rowHeight, this.width, this.separatorWidth);

            ctx.fillStyle = '#' + this.bgColor
            if (index % 2 == 0) {
                ctx.fillStyle = '#' + this.bgAlterColor
            }
            if (highlights.indexOf(value.team.name.toLocaleLowerCase()) != -1) {
                ctx.fillStyle = '#' + this.highlightedTeamColor
            }
            ctx.fillRect(0, titleHeight + (index + 1) * this.rowHeight + this.separatorWidth, this.width, this.rowHeight- this.separatorWidth);

            ctx.fillStyle = this.colorConfig[value.color]
            ctx.fillRect(0, titleHeight + (index + 1) * this.rowHeight + this.separatorWidth, this.cellWidth, this.rowHeight);
        })

        // vertical grid
        ctx.fillStyle = '#' + this.separatorColor
        ctx.fillRect(0, titleHeight, this.width, this.separatorWidth)
        ctx.fillRect(this.cellWidth, titleHeight + this.rowHeight,
                    this.separatorWidth, this.rowHeight * group.length)
        ctx.fillRect(this.width - this.cellWidth, titleHeight + this.rowHeight,
                    this.separatorWidth, this.rowHeight * group.length)
        ctx.fillRect(this.width - this.cellWidth * 2, titleHeight + this.rowHeight,
                    this.separatorWidth, this.rowHeight * group.length)
        ctx.fillRect(this.width - this.cellWidth * 3, titleHeight + this.rowHeight,
                    this.separatorWidth, this.rowHeight * group.length)
        ctx.fillRect(this.width - this.cellWidth * 4, titleHeight + this.rowHeight,
                    this.separatorWidth, this.rowHeight * group.length)
        ctx.fillRect(this.width - this.cellWidth * 5, titleHeight + this.rowHeight,
                    this.separatorWidth, this.rowHeight * group.length)
        ctx.fillRect(this.width - this.cellWidth * 6, titleHeight + this.rowHeight,
                    this.separatorWidth, this.rowHeight * group.length)
        ctx.fillRect(this.width - this.cellWidth * 7, titleHeight + this.rowHeight,
                    this.separatorWidth, this.rowHeight * group.length)

        // bottom separator
        ctx.fillStyle = '#' + this.separatorColor
        ctx.fillRect(0, titleHeight + (group.length + 1) * this.rowHeight, this.width, titleHeight + this.separatorWidth);

        ctx.textBaseline = 'alphabetic'

        // header
        const textY = titleHeight + this.rowHeight - this.rowHeight / 3.25
        ctx.font = '900 ' + this.fontSize + "px Roboto"
        ctx.fillStyle = '#' + this.teamStatsColor
        ctx.textAlign = 'start'
        ctx.fillText("Команда", this.cellWidth + (this.cellWidth - this.flagWidth) / 2, textY)
        ctx.textAlign = 'center'
        ctx.fillText("О", this.width - this.cellWidth / 2, textY)
        ctx.fillText("Проп", this.width - this.cellWidth / 2 - this.cellWidth, textY)
        ctx.fillText("Заб", this.width - this.cellWidth / 2 - this.cellWidth * 2, textY)
        ctx.fillText("П", this.width - this.cellWidth / 2 - this.cellWidth * 3, textY)
        ctx.fillText("Н", this.width - this.cellWidth / 2 - this.cellWidth * 4, textY)
        ctx.fillText("В", this.width - this.cellWidth / 2 - this.cellWidth * 5, textY)
        ctx.fillText("М", this.width - this.cellWidth / 2 - this.cellWidth * 6, textY)

        ctx.font = this.fontSize + "px Roboto"

        group.forEach((standingRow: StandingRow, index: number) => {
            const textY = titleHeight + (index + 2) * this.rowHeight - this.rowHeight / 3.25

            // Team name
            ctx.fillStyle = '#' + this.teamNameColor
            ctx.textAlign = 'start'
            ctx.fillText(this.generateTeamName(standingRow.team.name, replacementRules), this.cellWidth + (this.cellWidth - this.flagWidth) / 2, textY)

            // Team Stats
            ctx.fillStyle = '#' + this.teamStatsColor
            ctx.textAlign = 'center'
            ctx.fillText(index + 1, this.cellWidth / 2, textY)
            ctx.font = '700 ' + this.fontSize + "px Roboto"
            ctx.fillText(standingRow.points, this.width - this.cellWidth / 2, textY)
            ctx.font = this.fontSize + "px Roboto"
            ctx.fillText(standingRow.goalsAgainst, this.width - this.cellWidth / 2 - this.cellWidth * 1, textY)
            ctx.fillText(standingRow.goalsFor, this.width - this.cellWidth / 2 - this.cellWidth * 2, textY)
            ctx.fillText(standingRow.lost, this.width - this.cellWidth / 2 - this.cellWidth * 3, textY)
            ctx.fillText(standingRow.drawn, this.width - this.cellWidth / 2 - this.cellWidth * 4, textY)
            ctx.fillText(standingRow.won, this.width - this.cellWidth / 2 - this.cellWidth * 5, textY)
            ctx.fillText(standingRow.matches, this.width - this.cellWidth / 2 - this.cellWidth * 6, textY)
        })

        return groupImage
    }

    private generateTeamName(teamName: string, replacementRules: {[key: string]: string}): string {
        return replacementRules[teamName] || teamName
    }
}

function normalizeArray<T>(array: Array<T>, indexKey: keyof T) {
    const normalizedObject: any = {}
    for (let i = 0; i < array.length; i++) {
        const key = array[i][indexKey]
        const current = normalizedObject[key]
        if (current instanceof Array) {
            current.push(array[i])
        } else {
            normalizedObject[key] = [array[i]]
        }

    }
    return normalizedObject as { [key: string]: T[] }
}
