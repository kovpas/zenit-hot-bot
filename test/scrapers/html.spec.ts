import { Promise } from 'core-js'
import { HTMLScraper } from '../../src/scrapers/html'
import { HTTPClientMock } from '../mocks/scrapers/httpclient'
import { URL } from 'url'
import { HTMLDocument } from 'libxmljs'
import winston from "winston"
import fs from 'fs'

import { expect } from 'chai'
import 'mocha'

describe('Parse html', () => {
    const mockLogger = winston.createLogger({ transports: [new winston.transports.Stream({ stream: fs.createWriteStream('/dev/null') })] })
    const mockHttpClient = new HTTPClientMock()
    const testUrl = new URL('http://test')

    var sutParser: HTMLScraper

    beforeEach(() => {
        sutParser = new HTMLScraper(mockLogger, mockHttpClient, 'utf-8')
    })

    it('Should parse a small document', () => {
        mockHttpClient.getHandler = (_url: string, _options: any) => {
            const content = fs.readFileSync('./test/scrapers/data/simple.html')
            return Promise.resolve(content)
        }

        return sutParser.rootNodeForPage(testUrl).then((rootNode: HTMLDocument) => {
            expect(rootNode).to.be.a('Document')
            expect(rootNode.find('/html/head').length).to.equal(1)
            expect(rootNode.find('/html/body').length).to.equal(1)
        })
    })

    it('Should parse a big document', () => {
        mockHttpClient.getHandler = (_url: string, _options: any) => {
            const content = fs.readFileSync('./test/scrapers/data/rpl.html')
            return Promise.resolve(content)
        }

        return sutParser.rootNodeForPage(testUrl).then((rootNode: HTMLDocument) => {
            expect(rootNode).to.be.a('Document')
            expect(rootNode.find('//div[@id="middle_col"]').length).to.equal(1)
        })
    })

    it('Should throw an error', () => {
        mockHttpClient.getHandler = (_url: string, _options: any) => {
            return Promise.reject(new Error("test error"))
        }

        return sutParser.rootNodeForPage(testUrl).then((_rootNode: HTMLDocument) => {
            expect.fail(".then should not be called")
        }).catch((error: Error) => {
            expect(error).to.be.an('Error')
        })
    })

    it('Should throw an error for an empty document', () => {
        mockHttpClient.getHandler = (_url: string, _options: any) => {
            const content = fs.readFileSync('./test/scrapers/data/empty.html')
            return Promise.resolve(content)
        }

        return sutParser.rootNodeForPage(testUrl).then((rootNode: HTMLDocument) => {
            expect.fail(".then should not be called")
        }).catch((error: Error) => {
            expect(error).to.be.an('Error')
        })
    })

})