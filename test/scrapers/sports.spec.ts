import { Promise } from 'core-js'
import { SportsScraper } from '../../src/scrapers/sports'
import { HTMLScrapingMock } from '../mocks/scrapers/html'
import { JSONScrapingMock } from '../mocks/scrapers/json'
import winston from "winston"
import fs from 'fs'

import { expect } from 'chai'
import 'mocha'
import { StandingRow } from '../../src/models/league';
import { Team } from '../../src/models/team';

describe('Parse sportsru document', () => {
    const mockLogger = winston.createLogger({ transports: [new winston.transports.Stream({ stream: fs.createWriteStream('/dev/null') })] })
    const mockHtmlScraper = new HTMLScrapingMock()
    const mockJSONScraper = new JSONScrapingMock()
    const leagueId = 31

    var sutSportsScraper: SportsScraper

    beforeEach(() => {
        sutSportsScraper = new SportsScraper(mockLogger, mockHtmlScraper, mockJSONScraper)
    })

    it('Should return a league object', async () => {
        mockJSONScraper.loadJsonHandler = async (url) => {
            let path = ""
            if (url.path == "/stat/export/iphone/tournament_info.json?tournament=31") {
                path = "./test/scrapers/data/tournament_info.json"
            } else if (url.path == "/stat/export/iphone/tournament_stat_seasons.json?tournament=31" ) {
                path = "./test/scrapers/data/tournament_stat_seasons.json"
            } else if (url.path == "/stat/export/iphone/stat_table.json?id=6886") {
                path = "./test/scrapers/data/stat_table.json"
            } else if (url.path == "/stat/export/iphone/tournament_calendar.json?tournament=31") {
                path = "./test/scrapers/data/tournament_calendar.json"
            }
            return JSON.parse(fs.readFileSync(path).toString('utf-8'))
        }

        const league = await sutSportsScraper.getLeague(leagueId);
        const firstTeam = new Team("First Team", []);
        const secondTeam = new Team("Second Team", []);
        const expectedFirstStandingRow = new StandingRow(1, "group 1", firstTeam, 3, 4, 5, 6, 7, 8, 9, 1);
        const expectedSecondStandingRow = new StandingRow(2, "group 2", secondTeam, 13, 14, 15, 16, 17, 18, 19, 11);
        expect(league.name).to.equal('League Name')
        expect(league.year).to.equal('2018/2019')
        expect(league.standings[0].team.name).to.equal(firstTeam.name);
        expect(league.standings[0].matches).to.equal(expectedFirstStandingRow.matches);
        expect(league.standings[0].won).to.equal(expectedFirstStandingRow.won);
        expect(league.standings[0].drawn).to.equal(expectedFirstStandingRow.drawn);
        expect(league.standings[0].lost).to.equal(expectedFirstStandingRow.lost);
        expect(league.standings[0].goalsFor).to.equal(expectedFirstStandingRow.goalsFor);
        expect(league.standings[0].goalsAgainst).to.equal(expectedFirstStandingRow.goalsAgainst);
        expect(league.standings[0].points).to.equal(expectedFirstStandingRow.points);
        expect(league.standings[0].color).to.equal(expectedFirstStandingRow.color);
        expect(league.standings[1].team.name).to.equal(secondTeam.name);
        expect(league.standings[1].matches).to.equal(expectedSecondStandingRow.matches);
        expect(league.standings[1].won).to.equal(expectedSecondStandingRow.won);
        expect(league.standings[1].drawn).to.equal(expectedSecondStandingRow.drawn);
        expect(league.standings[1].lost).to.equal(expectedSecondStandingRow.lost);
        expect(league.standings[1].goalsFor).to.equal(expectedSecondStandingRow.goalsFor);
        expect(league.standings[1].goalsAgainst).to.equal(expectedSecondStandingRow.goalsAgainst);
        expect(league.standings[1].points).to.equal(expectedSecondStandingRow.points);
        expect(league.standings[1].color).to.equal(expectedSecondStandingRow.color);
    })

    it('Should throw an error', async () => {
        mockJSONScraper.loadJsonHandler = async (url) => {
            throw new Error("some error")
        }

        try {
            await sutSportsScraper.getLeague(leagueId);
            expect.fail(".then should not be called");
        }
        catch (error) {
            expect(error).to.be.an('Error');
        }
    })

})
