import fs from 'fs'
import 'mocha'

import { MatchesImageGenerator } from '../../src/image_generators/matches';
import { Team } from '../../src/models/team';
import { Match, MatchType } from '../../src/models/match';

describe('Matches Image Generator', () => {
    var sutMatchesImageGenerator: MatchesImageGenerator

    beforeEach(() => {
        sutMatchesImageGenerator = new MatchesImageGenerator()
    })

    it('Should generate a league image', () => {
        const team1 = new Team("Зенит", [])
        const team2 = new Team("ФК Краснодар", [])
        const team3 = new Team("ФК Ростов", [])
        const team4 = new Team("Спартак", [])
        const team5 = new Team("ЦСКА", [])
        const team6 = new Team("ФК Оренбург", [])
        const team7 = new Team("Ахмат", [])
        const team8 = new Team("Рубин", [])

        const match1 = new Match(0, MatchType.League, new Date(), team1, team2, false, 1, 0)
        const match2 = new Match(0, MatchType.NationalCup, new Date(), team3, team1, false, 1, 0)
        const match3 = new Match(0, MatchType.EuropaLeague, new Date(), team1, team6, false, 0, 0)
        const match4 = new Match(0, MatchType.ChampionsLeague, new Date(), team7, team1, false, 10, 12)
        const match5 = new Match(0, MatchType.League, new Date(), team1, team8, false, 0, 0, 3, 4)
        const match6 = new Match(0, MatchType.EuropaLeague, new Date(), team1, team7, false, 0, 0, 4, 3)
        const match7 = new Match(0, MatchType.League, new Date(), team1, team4, false)
        const match8 = new Match(0, MatchType.League, new Date(), team5, team1, false)
        const matches = [match1, match2, match3, match4, match5, match6, match7, match8]

        return sutMatchesImageGenerator.generateImageBufferFromMatches(matches, {}, team1).then((buffer: Buffer) => {
            const out = fs.createWriteStream(`tmp/matches.png`)
            out.write(buffer);
            out.end();

            console.log(buffer)
        })
    })
})