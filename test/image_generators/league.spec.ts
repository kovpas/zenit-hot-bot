import { Promise } from 'core-js'
import { expect } from 'chai'
import fs from 'fs'
import 'mocha'

import { LeagueImageGenerator } from '../../src/image_generators/league';
import { Team } from '../../src/models/team';
import { League, StandingRow } from '../../src/models/league';

describe('League Image Generator', () => {
    var sutLeagueImageGenerator: LeagueImageGenerator

    beforeEach(() => {
        sutLeagueImageGenerator = new LeagueImageGenerator()
    })

    it('Should generate a league image', async () => {
        const team1 = new Team("Зенит", [])
        const standingRow1 = new StandingRow(1, "", team1, 8, 7, 1, 0, 17, 5, 22, 1)
        const team2 = new Team("ФК Краснодар", [])
        const standingRow2 = new StandingRow(2, "", team2, 8, 5, 1, 2, 14, 6, 16, 1)
        const team3 = new Team("ФК Ростов", [])
        const standingRow3 = new StandingRow(3, "", team3, 8, 4, 3, 1, 9, 3, 15, 1)
        const team4 = new Team("Спартак", [])
        const standingRow4 = new StandingRow(4, "", team4, 8, 4, 3, 1, 7, 4, 15, 2)
        const team5 = new Team("ЦСКА", [])
        const standingRow5 = new StandingRow(5, "", team5, 8, 3, 4, 1, 13, 4, 13, 2)
        const team6 = new Team("ФК Оренбург", [])
        const standingRow6 = new StandingRow(6, "", team6, 8, 3, 2, 3, 10, 8, 11)
        const team7 = new Team("Ахмат", [])
        const standingRow7 = new StandingRow(7, "", team7, 8, 3, 2, 3, 9, 10, 11)
        const team8 = new Team("Рубин", [])
        const standingRow8 = new StandingRow(8, "", team8, 8, 2, 5, 1, 9, 8, 11)
        const team9 = new Team("Динамо", [])
        const standingRow9 = new StandingRow(9, "", team9, 8, 2, 4, 2, 9, 6, 10)
        const team10 = new Team("Арсенал", [])
        const standingRow10 = new StandingRow(10, "", team10, 8, 2, 3, 3, 9, 8, 9)
        const team11 = new Team("Локомотив", [])
        const standingRow11 = new StandingRow(11, "", team11, 8, 2, 3, 3, 8, 10, 9)
        const team12 = new Team("Урал", [])
        const standingRow12 = new StandingRow(12, "", team12, 8, 2, 2, 4, 8, 15, 8)
        const team13 = new Team("Крылья Советов", [])
        const standingRow13 = new StandingRow(13, "", team13, 8, 2, 1, 5, 2, 12, 7, 3)
        const team14 = new Team("Анжи", [])
        const standingRow14 = new StandingRow(14, "", team14, 8, 2, 0, 6, 4, 14, 6, 3)
        const team15 = new Team("ФК Уфа", [])
        const standingRow15 = new StandingRow(15, "", team15, 8, 1, 2, 5, 4, 11, 5, 4)
        const team16 = new Team("Енисей", [])
        const standingRow16 = new StandingRow(16, "", team16, 8, 1, 2, 5, 3, 11, 5, 4)
        const standings = [standingRow1, standingRow2, standingRow3, standingRow4, standingRow5, standingRow6, standingRow7, standingRow8, standingRow9, standingRow10, standingRow11, standingRow12, standingRow13, standingRow14, standingRow15, standingRow16]

        const league = new League("test", "2018/19", [], standings)
        const buffer = await sutLeagueImageGenerator.generateImageFromLeague(league, {});
        const out = fs.createWriteStream(`tmp/league.png`)
        out.write(buffer);
        out.end();
    })

    it('Should generate a league image with groups', async () => {
        const team1 = new Team("Зенит", [])
        const standingRow1 = new StandingRow(1, "Group A", team1, 8, 7, 1, 0, 17, 5, 22, 1)
        const team2 = new Team("ФК Краснодар", [])
        const standingRow2 = new StandingRow(2, "Group A", team2, 8, 5, 1, 2, 14, 6, 16, 1)
        const team3 = new Team("ФК Ростов", [])
        const standingRow3 = new StandingRow(3, "Group A", team3, 8, 4, 3, 1, 9, 3, 15, 1)
        const team4 = new Team("Спартак", [])
        const standingRow4 = new StandingRow(4, "Group A", team4, 8, 4, 3, 1, 7, 4, 15, 2)
        const team5 = new Team("ЦСКА", [])
        const standingRow5 = new StandingRow(1, "Group B", team5, 8, 3, 4, 1, 13, 4, 13, 2)
        const team6 = new Team("ФК Оренбург", [])
        const standingRow6 = new StandingRow(2, "Group B", team6, 8, 3, 2, 3, 10, 8, 11)
        const team7 = new Team("Ахмат", [])
        const standingRow7 = new StandingRow(3, "Group B", team7, 8, 3, 2, 3, 9, 10, 11)
        const team8 = new Team("Рубин", [])
        const standingRow8 = new StandingRow(4, "Group B", team8, 8, 2, 5, 1, 9, 8, 11)
        const team9 = new Team("Динамо", [])
        const standingRow9 = new StandingRow(9, "Group C", team9, 8, 2, 4, 2, 9, 6, 10)
        const team10 = new Team("Арсенал", [])
        const standingRow10 = new StandingRow(10, "Group C", team10, 8, 2, 3, 3, 9, 8, 9)
        const team11 = new Team("Локомотив", [])
        const standingRow11 = new StandingRow(11, "Group C", team11, 8, 2, 3, 3, 8, 10, 9)
        const team12 = new Team("Урал", [])
        const standingRow12 = new StandingRow(12, "Group C", team12, 8, 2, 2, 4, 8, 15, 8)
        const team13 = new Team("Крылья Советов", [])
        const standingRow13 = new StandingRow(13, "Group D", team13, 8, 2, 1, 5, 2, 12, 7, 3)
        const team14 = new Team("Анжи", [])
        const standingRow14 = new StandingRow(14, "Group D", team14, 8, 2, 0, 6, 4, 14, 6, 3)
        const team15 = new Team("ФК Уфа", [])
        const standingRow15 = new StandingRow(15, "Group D", team15, 8, 1, 2, 5, 4, 11, 5, 4)
        const team16 = new Team("Енисей", [])
        const standingRow16 = new StandingRow(16, "Group D", team16, 8, 1, 2, 5, 3, 11, 5, 4)
        const standings = [standingRow1, standingRow2, standingRow3, standingRow4, standingRow5, standingRow6, standingRow7, standingRow8, standingRow9, standingRow10, standingRow11, standingRow12, standingRow13, standingRow14, standingRow15, standingRow16]

        const league = new League("test", "2018/19", [], standings)
        const buffer = await sutLeagueImageGenerator.generateImageFromLeague(league, {});
        const out = fs.createWriteStream(`tmp/groups.png`);
        out.write(buffer);
        out.end();
    })
})