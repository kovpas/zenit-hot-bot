import { JSONScraping } from '../../../src/scrapers/json'
import { Url } from 'url'

export class JSONScrapingMock implements JSONScraping {
    loadJsonHandler?: ((url: Url) => Promise<any>)

    loadJson(url: Url): Promise<any> {
        if (this.loadJsonHandler == null) {
            throw new Error('loadJsonHandler not set')
        }
        return this.loadJsonHandler(url)
    }
}