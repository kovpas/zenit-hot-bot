import { HTTPClient } from '../../../src/scrapers/httpclient'

export class HTTPClientMock implements HTTPClient {
    getHandler?: ((url: string, options: any) => Promise<ArrayBuffer>)

    get(url: string, options: any): Promise<ArrayBuffer> {
        if (this.getHandler == null) {
            throw new Error('getHandler not set')
        }
        return this.getHandler(url, options)
    }
}