import { HTMLScraping } from '../../../src/scrapers/html'
import { Document } from 'libxmljs'
import { URL } from 'url'

export class HTMLScrapingMock implements HTMLScraping {
    rootNodeForPageHandler?: ((url: URL) => Promise<Document>)

    rootNodeForPage(url: URL): Promise<Document> {
        if (this.rootNodeForPageHandler == null) {
            throw new Error('rootNodeForPageHandler not set')
        }
        return this.rootNodeForPageHandler(url)
    }
}